#!/bin/bash


SITE_HOST=$(itomic_get_config environments.${ITOMIC_ENV}.hostname)
SSH_USER=$(itomic_get_config environments.${ITOMIC_ENV}.user)
DRUPAL_WEB_INSTALL=$(itomic_get_config environments.${ITOMIC_ENV}.drupal.web_install_dir)
[ -z "$DRUPAL_WEB_INSTALL" ] && DRUPAL_WEB_INSTALL="current/web"

ASK_COMMAND_DISPLAY_ALL='true'

for i in "$@"
do
  shift
  case $i in
    setup)
      echo "haven't done this yet"
    ;;
    grab:files) #grab from the files directory
      SITE_FILES="~/$DRUPAL_WEB_INSTALL/sites/default/files"
      LOCAL_FILES="web/sites/default/files";
      echo "Backing up local files"
      [ -d "$LOCAL_FILES" ] && ask_command "mv $LOCAL_FILES tmp/sitefiles-$(itomic_timestamp)"
      SITE_COPY_CMD="scp -Cr $SSH_USER@$SITE_HOST:$SITE_FILES $LOCAL_FILES"

      echo "Importing files from $SSH_USER@$SITE_HOST (you must have ssh access to this account)"

      ask_command $SITE_COPY_CMD
    ;;
    grab:db)
      database=$(itomic_get_config environments.${ITOMIC_ENV}.mysql.database)
      db_user=$(itomic_get_config environments.${ITOMIC_ENV}.mysql.user)
      db_pass=$(itomic_get_config environments.${ITOMIC_ENV}.mysql.password)
      TMP_DATABASE_FILE_LOCAL="tmp/$database-$(itomic_timestamp).sql"

      TMP_DATABASE_FILE_ON_SITE="~/$TMP_DATABASE_FILE_LOCAL"
      MYSQL_COMMAND="mysqldump -u$db_user -p'$db_pass' $database > $TMP_DATABASE_FILE_ON_SITE"
      SSH_COMMAND="ssh $SSH_USER@$SITE_HOST"
      echo -e "This must be run manually"
      echo "SSH into the server:"
      echo "$SSH_COMMAND"
      echo -e "          $MYSQL_COMMAND\n"

      # ask_clipboard "$MYSQL_COMMAND"

      echo "Then on dev machine:"

      DB_BACKUP_COPY_CMD="scp -C $SSH_USER@$SITE_HOST:$TMP_DATABASE_FILE_ON_SITE $TMP_DATABASE_FILE_LOCAL"

      echo $DB_BACKUP_COPY_CMD

    ;;
    grab:settings)
      SITE_CONFIG_FILE="~/$DRUPAL_WEB_INSTALL/sites/default/settings.php"
      LOCAL_CONFIG_FILE="./tmp/settings.${ITOMIC_ENV}.php"
      SITE_COPY_CMD="scp -C $SSH_USER@$SITE_HOST:$SITE_CONFIG_FILE $LOCAL_CONFIG_FILE"

      echo "Importing settings.php from $SSH_USER@$SITE_HOST:$SITE_CONFIG_FILE (you must have ssh access to this account)"
      ask_command $SITE_COPY_CMD

      #avoid permissions weirdness
      ask_command "chmod 644 $LOCAL_CONFIG_FILE"
    ;;
    extract:settings)
    echo "extract setting"
      #Extract settings from a downloaded settings file into the .itomic.yml file
      LOCAL_CONFIG_FILE="tmp/settings.${ITOMIC_ENV}.php"

      tmp_file=tmp/db-settings.${ITOMIC_ENV}.yml

      #ask_command currently doesnt work with redirection
      `php $ITOMIC_SHELL_ORIGIN/bin/show-database-settings.php $LOCAL_CONFIG_FILE > $tmp_file`
      database=$(read_yaml_setting $tmp_file database)
      user=$(read_yaml_setting $tmp_file user)
      password=$(read_yaml_setting $tmp_file password)

      command "cp .itomic.yml tmp/itomic.yml-$(itomic_timestamp)"

      write_yaml_setting .itomic.yml environments.${ITOMIC_ENV}.mysql.database $database
      write_yaml_setting .itomic.yml environments.${ITOMIC_ENV}.mysql.user $user
      write_yaml_setting .itomic.yml environments.${ITOMIC_ENV}.mysql.password $password
    ;;
    *)
      # unknown option
    ;;
  esac
done

#!/bin/bash


SITE_HOST=$(itomic_get_config environments.${ITOMIC_ENV}.hostname)
SSH_USER=$(itomic_get_config environments.${ITOMIC_ENV}.user)
DRUPAL_WEB_INSTALL=$(itomic_get_config environments.${ITOMIC_ENV}.drupal.web_install_dir)
[ -z "$DRUPAL_WEB_INSTALL" ] && DRUPAL_WEB_INSTALL="current/web"

ASK_COMMAND_DISPLAY_ALL='true'

for i in "$@"
do
  shift
  case $i in
    push:mysql-dump-config-file)
      db_password=$(itomic_get_config environments.${ITOMIC_ENV}.mysql.password)

      config_file_local="tmp/mysql-config"
      echo "[client]" > $config_file_local
      echo "password=\"${db_password}\"" >> $config_file_local
      cat $config_file_local
      ask_command "scp $config_file_local $SSH_USER@$SITE_HOST:~/.mysql-client-config"
    ;;
    grab:db)
      database=$(itomic_get_config environments.${ITOMIC_ENV}.mysql.database)
      db_user=$(itomic_get_config environments.${ITOMIC_ENV}.mysql.user)
      db_pass=$(itomic_get_config environments.${ITOMIC_ENV}.mysql.password)
      TMP_DATABASE_FILE_LOCAL="tmp/$database-$(itomic_timestamp).sql"

      TMP_DATABASE_FILE_ON_SITE="~/$TMP_DATABASE_FILE_LOCAL"
      MYSQL_COMMAND="mysqldump --defaults-extra-file=.mysql-client-config -u$db_user $database > $TMP_DATABASE_FILE_ON_SITE"
      SSH_COMMAND="ssh $SSH_USER@$SITE_HOST '$MYSQL_COMMAND'"
      echo -e "RUN dump command manually\n"
      echo -e "   $SSH_COMMAND\n\n"

      DB_BACKUP_COPY_CMD="scp -C $SSH_USER@$SITE_HOST:$TMP_DATABASE_FILE_ON_SITE $TMP_DATABASE_FILE_LOCAL"

      echo -e "Then run scp command manually\n\n"
      echo -e "   $DB_BACKUP_COPY_CMD\n\n"
      echo

    ;;  *)
      # unknown option
    ;;
  esac
done


itomic_parse_repository_user_name_branch() {
  REPO=$1
  REPO_DETAILS=$(echo $REPO | sed 's/\// /' | sed 's/#/ /')
  echo $REPO_DETAILS
}

itomic_timestamp() {
  date +%Y%m%d_%H%M%S
}

command() {
  $@
}

say_command() {
  echo "$@"
}

ask_command( ) {
    echo -e "\e[31mDo you want to run?:\e[0m $@"
    read answer
    if [ "$answer" == "y" ]; then
       echo "Running: "
       $@
    else
      echo "Not running"
    fi
}

ask_continue() {
  echo "Do you want to continue with this command?: (You must press CTRL-C to abort)"
  echo "$@"
  read answer
}

#params are provicer_url, $repo_user, $repo_name
enstarter_build_git_url() {
  echo "git@$1:$2/$3.git"
}

#pass in the yaml key (i.e. environments.uat.hostname)
itomic_get_config() {
  echo $(yaml get .itomic.yml $1)
}

read_yaml_setting() {
  yaml get $1 $2
}

write_yaml_setting() {
  output=$(yaml set $1 $2 $3 > tmp/.itomic.yml)
  mv tmp/.itomic.yml .itomic.yml
}
